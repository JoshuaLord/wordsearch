/********************************************************************************
** Form generated from reading UI file 'WordSearch.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WORDSEARCH_H
#define UI_WORDSEARCH_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WordSearchClass
{
public:
    QWidget *layoutWidget;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QLabel *maxLenLabel;
    QLabel *heightLabel;
    QLabel *minLenLabel;
    QLabel *widthLabel;
    QSpinBox *maxLenSpinBox;
    QSpinBox *widthSpinBox;
    QSpinBox *minLenSpinBox;
    QSpacerItem *horizontalSpacer;
    QSpinBox *heightSpinBox;
    QLabel *numWordLabel;
    QSpinBox *numWordSpinBox;
    QSpacerItem *verticalSpacer;
    QPushButton *generateButton;

    void setupUi(QWidget *WordSearchClass)
    {
        if (WordSearchClass->objectName().isEmpty())
            WordSearchClass->setObjectName(QString::fromUtf8("WordSearchClass"));
        WordSearchClass->resize(259, 179);
        layoutWidget = new QWidget(WordSearchClass);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 10, 241, 163));
        gridLayout_2 = new QGridLayout(layoutWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setSizeConstraint(QLayout::SetMaximumSize);
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        maxLenLabel = new QLabel(layoutWidget);
        maxLenLabel->setObjectName(QString::fromUtf8("maxLenLabel"));

        gridLayout->addWidget(maxLenLabel, 3, 0, 1, 1);

        heightLabel = new QLabel(layoutWidget);
        heightLabel->setObjectName(QString::fromUtf8("heightLabel"));

        gridLayout->addWidget(heightLabel, 4, 0, 1, 1);

        minLenLabel = new QLabel(layoutWidget);
        minLenLabel->setObjectName(QString::fromUtf8("minLenLabel"));

        gridLayout->addWidget(minLenLabel, 1, 0, 1, 1);

        widthLabel = new QLabel(layoutWidget);
        widthLabel->setObjectName(QString::fromUtf8("widthLabel"));

        gridLayout->addWidget(widthLabel, 5, 0, 1, 1);

        maxLenSpinBox = new QSpinBox(layoutWidget);
        maxLenSpinBox->setObjectName(QString::fromUtf8("maxLenSpinBox"));
        maxLenSpinBox->setMinimum(4);
        maxLenSpinBox->setMaximum(15);
        maxLenSpinBox->setValue(8);

        gridLayout->addWidget(maxLenSpinBox, 3, 2, 1, 1);

        widthSpinBox = new QSpinBox(layoutWidget);
        widthSpinBox->setObjectName(QString::fromUtf8("widthSpinBox"));
        widthSpinBox->setMinimum(1);
        widthSpinBox->setMaximum(100);
        widthSpinBox->setValue(20);

        gridLayout->addWidget(widthSpinBox, 5, 2, 1, 1);

        minLenSpinBox = new QSpinBox(layoutWidget);
        minLenSpinBox->setObjectName(QString::fromUtf8("minLenSpinBox"));
        minLenSpinBox->setMinimum(3);
        minLenSpinBox->setMaximum(14);
        minLenSpinBox->setValue(4);

        gridLayout->addWidget(minLenSpinBox, 1, 2, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 3, 1, 2, 1);

        heightSpinBox = new QSpinBox(layoutWidget);
        heightSpinBox->setObjectName(QString::fromUtf8("heightSpinBox"));
        heightSpinBox->setMinimum(1);
        heightSpinBox->setMaximum(100);
        heightSpinBox->setValue(20);

        gridLayout->addWidget(heightSpinBox, 4, 2, 1, 1);

        numWordLabel = new QLabel(layoutWidget);
        numWordLabel->setObjectName(QString::fromUtf8("numWordLabel"));

        gridLayout->addWidget(numWordLabel, 0, 0, 1, 1);

        numWordSpinBox = new QSpinBox(layoutWidget);
        numWordSpinBox->setObjectName(QString::fromUtf8("numWordSpinBox"));
        numWordSpinBox->setMinimum(1);
        numWordSpinBox->setMaximum(20);
        numWordSpinBox->setValue(10);

        gridLayout->addWidget(numWordSpinBox, 0, 2, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 18, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 1, 0, 1, 1);

        generateButton = new QPushButton(layoutWidget);
        generateButton->setObjectName(QString::fromUtf8("generateButton"));

        gridLayout_2->addWidget(generateButton, 2, 0, 1, 1);


        retranslateUi(WordSearchClass);

        QMetaObject::connectSlotsByName(WordSearchClass);
    } // setupUi

    void retranslateUi(QWidget *WordSearchClass)
    {
        WordSearchClass->setWindowTitle(QCoreApplication::translate("WordSearchClass", "WordSearch", nullptr));
        maxLenLabel->setText(QCoreApplication::translate("WordSearchClass", "Maximum Word Length", nullptr));
        heightLabel->setText(QCoreApplication::translate("WordSearchClass", "Height", nullptr));
        minLenLabel->setText(QCoreApplication::translate("WordSearchClass", "Minimum Word Length", nullptr));
        widthLabel->setText(QCoreApplication::translate("WordSearchClass", "Width", nullptr));
        numWordLabel->setText(QCoreApplication::translate("WordSearchClass", "Number of Words", nullptr));
        generateButton->setText(QCoreApplication::translate("WordSearchClass", "Generate", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WordSearchClass: public Ui_WordSearchClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WORDSEARCH_H
