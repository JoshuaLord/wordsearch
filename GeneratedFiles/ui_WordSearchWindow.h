/********************************************************************************
** Form generated from reading UI file 'WordSearchWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WORDSEARCHWINDOW_H
#define UI_WORDSEARCHWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WordSearchWindow
{
public:
    QTableWidget *searchTable;
    QTableWidget *wordTable;

    void setupUi(QWidget *WordSearchWindow)
    {
        if (WordSearchWindow->objectName().isEmpty())
            WordSearchWindow->setObjectName(QString::fromUtf8("WordSearchWindow"));
        WordSearchWindow->resize(354, 517);
        searchTable = new QTableWidget(WordSearchWindow);
        searchTable->setObjectName(QString::fromUtf8("searchTable"));
        searchTable->setGeometry(QRect(0, 0, 291, 192));
        searchTable->viewport()->setProperty("cursor", QVariant(QCursor(Qt::OpenHandCursor)));
        searchTable->setLineWidth(1);
        searchTable->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        searchTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        searchTable->setAutoScroll(true);
        searchTable->setAutoScrollMargin(0);
        searchTable->setShowGrid(false);
        searchTable->setWordWrap(false);
        searchTable->setCornerButtonEnabled(false);
        searchTable->horizontalHeader()->setVisible(false);
        searchTable->verticalHeader()->setVisible(false);
        wordTable = new QTableWidget(WordSearchWindow);
        wordTable->setObjectName(QString::fromUtf8("wordTable"));
        wordTable->setGeometry(QRect(0, 300, 291, 192));
        wordTable->setShowGrid(false);
        wordTable->horizontalHeader()->setVisible(false);
        wordTable->verticalHeader()->setVisible(false);

        retranslateUi(WordSearchWindow);

        QMetaObject::connectSlotsByName(WordSearchWindow);
    } // setupUi

    void retranslateUi(QWidget *WordSearchWindow)
    {
        WordSearchWindow->setWindowTitle(QCoreApplication::translate("WordSearchWindow", "WordSearchWindow", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WordSearchWindow: public Ui_WordSearchWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WORDSEARCHWINDOW_H
