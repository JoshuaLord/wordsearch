#include "stdafx.h"
#include "WordSearch.h"
#include "WordSearchWindow.h"

WordSearch::WordSearch(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	this->setFixedSize(this->size());
	this->setWindowTitle(QString("Word Search"));
}

void WordSearch::on_generateButton_clicked() {
	int numWords = ui.numWordSpinBox->value();
	int minWordLen = ui.minLenSpinBox->value();
	int maxWordLen = ui.maxLenSpinBox->value();
	int height = ui.heightSpinBox->value();
	int width = ui.widthSpinBox->value();

	WordSearchWindow *wsWindow = new WordSearchWindow(NULL, numWords, minWordLen, maxWordLen, height, width);
	
	wsWindow->show();

}