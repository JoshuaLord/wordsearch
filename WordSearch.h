#pragma once

#include <QtWidgets/QWidget>
#include "ui_WordSearch.h"
#include "WordSearchWindow.h"

class WordSearch : public QWidget
{
	Q_OBJECT

public:
	WordSearch(QWidget *parent = Q_NULLPTR);

private:
	Ui::WordSearchClass ui;

private slots:
	void on_generateButton_clicked();
};
