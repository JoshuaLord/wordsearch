#include "stdafx.h"
#include "WordSearchWindow.h"
#include <string>

void print(std::string s) {
	wchar_t t[100];
	mbstowcs(t, s.c_str(), strlen(s.c_str()) + 1);
	OutputDebugString(t);
}

void print(std::string s, int i) {
	print(s + ": " + std::to_string(i) + "\n");
}

WordSearchWindow::WordSearchWindow(QWidget *parent, int numWord, int minWordLen, int maxWordLen, int height, int width)
	: numWord(numWord), minWordLen(minWordLen), maxWordLen(maxWordLen)
{
	ui.setupUi(this);
	this->setWindowTitle(QString("Word Search"));

	ui.searchTable->setRowCount(height);
	ui.searchTable->setColumnCount(width);
	ui.searchTable->resizeColumnsToContents();
	ui.searchTable->resizeRowsToContents();
	ui.searchTable->setShowGrid(true);

	int sColWidth = ui.searchTable->columnWidth(0);
	int sRowHeight = ui.searchTable->rowHeight(0);

	ui.searchTable->setFixedSize(width * sColWidth, height * sRowHeight);
	
	int wordTableRows = (numWord % 3) == 0 ? numWord / 3 : (numWord / 3) + 1;
	print("Num", wordTableRows);
	ui.wordTable->setRowCount(wordTableRows);
	ui.wordTable->setColumnCount(3);
	ui.wordTable->resizeColumnsToContents();
	ui.wordTable->resizeRowsToContents();
	ui.wordTable->setShowGrid(true);

	int wColWidth = ui.wordTable->columnWidth(0);
	int wRowHeight = ui.wordTable->rowHeight(0);

	ui.wordTable->setFixedSize(width * wColWidth, height * wRowHeight);

	this->setFixedSize(width * sColWidth, ui.searchTable->height() + ui.wordTable->height() + 20);

	userFinished = false;
}

WordSearchWindow::~WordSearchWindow()
{
}
