#pragma once

#include <QWidget>
#include "ui_WordSearchWindow.h"

class WordSearchWindow : public QWidget, public Ui::WordSearchWindow
{
	Q_OBJECT

public:
	WordSearchWindow(QWidget *parent, int numWord, int minWordLen, int maxWordLen, int height, int width);
	~WordSearchWindow();	

	bool getUserFinished() 
	{
		return userFinished;
	}

private:
	Ui::WordSearchWindow ui;

	int numWord;
	int minWordLen;
	int maxWordLen;

	bool userFinished;

	std::vector<QString> wordList;
};
